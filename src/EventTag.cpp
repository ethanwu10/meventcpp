#include "mevent/EventTag.h"

using namespace MEvent;

EventTag EventTag::createTag(std::string name, EventTag base)
{
	return EventTag(base.tag + getSeparator() + name);
}

EventTag EventTag::createTag(std::string name)
{
	return createTag(name, getRoot());
}

const EventTag& EventTag::getRoot()
{
	static EventTag Root("");
	return Root;
}

const std::string& EventTag::getSeparator()
{
	static std::string Separator(".");
	return Separator;
}
