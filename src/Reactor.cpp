#include "mevent/Reactor.h"

using namespace MEvent;

Reactor::Reactor(EventDispatcher& dispatcher) : dispatcher(dispatcher)
{
	dispatcher.addTag(getTags().Stop, std::make_shared<StopHandler>(*this), true);
}

void Reactor::sendMessage(const EventTag& tag, std::shared_ptr<EventData> data, uint8_t priority)
{
	queue.push(QueuedEvent(tag, std::move(data), priority));
}

void Reactor::sendMessage(const EventTag& tag, Reactor::Priority_t priority)
{
	sendMessage(tag, std::make_shared<EventData>(), priority);
}

void Reactor::registerListener(const EventTag& tag, const std::shared_ptr<EventHandler>& handler, bool bubbles)
{
	dispatcher.addTag(tag, handler, bubbles);
}

void Reactor::run()
{
	running = true;
	sendMessage(getTags().Started, 0);
	while (!shouldStopNow)
	{
		if (queue.size()) // There's something in the queue
		{
			QueuedEvent event(std::move(const_cast<QueuedEvent&>(queue.top())));
			// Cast away const because we don't care if we alter the queue's correctness at this point
			// as we pop off this element next anyways
			queue.pop();
			dispatcher.dispatchTag(event.tag, *event.data);
		}
		else
		{
			// TODO: on idle
			if (shouldStop)
				break;
		}
	}
	running = false;
}

bool Reactor::isRunning()
{
	return running;
}

Reactor::Tags::Tags() : Root(EventTag::createTag("Reactor")),
                        Stop(EventTag::createTag("Stop", Root)),
                        StopNow(EventTag::createTag("Now", Stop)),
                        Started(EventTag::createTag("Started", Root))
{}

const Reactor::Tags& Reactor::getTags()
{
	static Tags tags;
	return tags;
}

void Reactor::StopHandler::handle(__attribute__((__unused__)) EventData& data, const EventTag& tag)
{
	parent.shouldStop = true;
	parent.shouldStopNow = tag == Reactor::getTags().StopNow;
}

Reactor::QueuedEvent::QueuedEvent(QueuedEvent&& other) : tag(std::move(other.tag)),
                                                         data(std::move(other.data)),
                                                         priority(std::move(other.priority))
{}

Reactor::QueuedEvent& Reactor::QueuedEvent::operator=(Reactor::QueuedEvent&& other)
{
	tag = std::move(other.tag);
	data = std::move(other.data);
	priority = std::move(other.priority);
	return *this;
}
