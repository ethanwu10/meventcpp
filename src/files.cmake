macro(add_source file)
    set(SOURCE_FILES ${SOURCE_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro(add_source)

add_source(EventTag.cpp)
add_source(EventDispatcher_STL.cpp)
add_source(Reactor.cpp)
