#include "mevent/EventDispatcher_STL.h"

#include "mevent/EventTag.h"
#include <map>
#include <vector>
#include <memory>

using namespace std;

MEvent::EventDispatcher_STL::EventDispatcher_STL()
{
}

void MEvent::EventDispatcher_STL::addTag(const EventTag& tag, shared_ptr<EventHandler> handler, bool bubbles)
{
	string tagStr = tag.getString();
	// TODO: Deduplicate
	const size_t tagLen = tagStr.size();
	const size_t SeparatorSize = EventTag::getSeparator().size();
	size_t pos = 0, posEnd;
	LUTNode* currNode = nullptr;
	bool isBottom = false;
	do
	{
		posEnd = tagStr.find(EventTag::getSeparator(), pos);
		if (posEnd == string::npos) // Last tag now
		{
			posEnd = tagLen; // 'Fix' the end to be one past end of string tag
			isBottom = true;
		}
		string currTag = tagStr.substr(pos, posEnd - pos);
		if (currNode == nullptr)
		{
			currNode = &LUTRoot;
		}
		else
		{
			currNode = &(currNode->children[currTag]);
		}

		if (isBottom)
		{
			if (bubbles)
			{
				currNode->bubblingHandlers.push_back(handler);
			}
			else
			{
				currNode->explicitHandlers.push_back(handler);
			}
		}

		pos = posEnd + SeparatorSize;
	} while (!isBottom);
}

void MEvent::EventDispatcher_STL::dispatchTag(const EventTag& tag, EventData& data)
{
	string tagStr = tag.getString();
	// TODO: Deduplicate
	const size_t tagLen = tagStr.size();
	const size_t SeparatorSize = EventTag::getSeparator().size();
	size_t pos = 0, posEnd;
	LUTNode* currNode = nullptr;
	bool isBottom = false;
	do
	{
		posEnd = tagStr.find(EventTag::getSeparator(), pos);
		if (posEnd == string::npos) // Last tag now
		{
			posEnd = tagLen; // 'Fix' the end to be one past end of string tag
			isBottom = true;
		}
		string currTag = tagStr.substr(pos, posEnd - pos);
		if (currNode == nullptr)
		{
			currNode = &LUTRoot;
		}
		else
		{
			if (!currNode->children.count(currTag)) // Does not have current tag node; we're done
				break;
			currNode = &(currNode->children[currTag]);
		}

		for (auto& handler : currNode->bubblingHandlers)
		{
			handler->handle(data, tag);
		}
		if (isBottom)
		{
			for (auto& handler : currNode->explicitHandlers)
			{
				handler->handle(data, tag);
			}
		}

		pos = posEnd + SeparatorSize;
	} while (!isBottom);
}
