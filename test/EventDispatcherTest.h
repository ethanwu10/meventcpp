#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "mevent/EventDispatcher.h"
#include "mevent/EventHandler.h"
#include "mevent/EventTag.h"

#include <list>
#include <memory>

template <typename T>
class EventDispatcherTest : public ::testing::Test
{
protected:

	EventDispatcherTest();

	std::unique_ptr<MEvent::EventDispatcher> dispatcher;

	class MockEventHandler : public MEvent::EventHandler
	{
	public:

		MOCK_METHOD2(handle, void(MEvent::EventData& data, const MEvent::EventTag& tag));

	};

	std::shared_ptr<MockEventHandler> eh;

	class DummyEventData : public MEvent::EventData
	{
	public:

		DummyEventData() = delete;

		static DummyEventData create()
		{
			return DummyEventData(curr_id++);
		}

		inline bool operator==(const DummyEventData& that) const { return id == that.id; }

	private:

		explicit DummyEventData(unsigned long id) : id(id) {}

		static unsigned long curr_id;

		unsigned long id;
	};

	MEvent::EventTag Tag1_1;
	MEvent::EventTag Tag2_1;
	MEvent::EventTag Tag2_2;
};

template <typename T>
EventDispatcherTest<T>::EventDispatcherTest() : dispatcher(new T),
                                                eh(new MockEventHandler),
                                                Tag1_1(MEvent::EventTag::createTag("1")),
                                                Tag2_1(MEvent::EventTag::createTag("1", Tag1_1)),
                                                Tag2_2(MEvent::EventTag::createTag("2", Tag1_1))
{}

template <typename T>
unsigned long EventDispatcherTest<T>::DummyEventData::curr_id = 0;

/////////////////////////////////////////////////////////////////////////
// TESTS
/////////////////////////////////////////////////////////////////////////

using namespace ::testing;

TYPED_TEST_CASE_P(EventDispatcherTest);

TYPED_TEST_P(EventDispatcherTest, DirectDispatch)
{
	this->dispatcher->addTag(this->Tag1_1, this->eh);
	typename TestFixture::DummyEventData eventData = TestFixture::DummyEventData::create();
	EXPECT_CALL(*this->eh, handle(Ref(eventData), this->Tag1_1))
			.Times(1);
	this->dispatcher->dispatchTag(this->Tag1_1, eventData);
}

TYPED_TEST_P(EventDispatcherTest, Bubbles)
{
	this->dispatcher->addTag(this->Tag1_1, this->eh, true);
	typename TestFixture::DummyEventData eventData = TestFixture::DummyEventData::create();
	EXPECT_CALL(*this->eh, handle(Ref(eventData), this->Tag2_1))
			.Times(1);
	this->dispatcher->dispatchTag(this->Tag2_1, eventData);
}

TYPED_TEST_P(EventDispatcherTest, DisableBubbling)
{
	this->dispatcher->addTag(this->Tag1_1, this->eh, false);
	typename TestFixture::DummyEventData eventData = TestFixture::DummyEventData::create();
	EXPECT_CALL(*this->eh, handle(_, _)).Times(0);
	this->dispatcher->dispatchTag(this->Tag2_1, eventData);
	EXPECT_CALL(*this->eh, handle(Ref(eventData), this->Tag1_1))
			.Times(1);
	this->dispatcher->dispatchTag(this->Tag1_1, eventData);
}

TYPED_TEST_P(EventDispatcherTest, DoesNotBubbleDown)
{
	this->dispatcher->addTag(this->Tag2_1, this->eh, true);
	typename TestFixture::DummyEventData eventData = TestFixture::DummyEventData::create();
	EXPECT_CALL(*this->eh, handle(_, _)) .Times(0);
	this->dispatcher->dispatchTag(this->Tag1_1, eventData);
}

TYPED_TEST_P(EventDispatcherTest, DoesNotBubbleSideways)
{
	this->dispatcher->addTag(this->Tag2_1, this->eh, true);
	typename TestFixture::DummyEventData eventData = TestFixture::DummyEventData::create();
	EXPECT_CALL(*this->eh, handle(_, _)) .Times(0);
	this->dispatcher->dispatchTag(this->Tag2_2, eventData);
}

REGISTER_TYPED_TEST_CASE_P(EventDispatcherTest,
                           DirectDispatch,
                           Bubbles,
                           DisableBubbling,
                           DoesNotBubbleDown,
                           DoesNotBubbleSideways);
