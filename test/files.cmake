macro(add_test_source file)
    set(TEST_SOURCE_FILES ${TEST_SOURCE_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro(add_test_source)

add_test_source(EventTagTest.cpp)
add_test_source(EventDispatcherTest.h)
add_test_source(EventDispatcher_STLTest.cpp)
add_test_source(ReactorTest.cpp)
