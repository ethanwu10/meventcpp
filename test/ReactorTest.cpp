#include "mevent/Reactor.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "mevent/EventDispatcher.h"
#include "mevent/EventDispatcher_STL.h"

using namespace ::testing;
using namespace MEvent;

class ReactorTest : public ::testing::Test
{

public: // To appease CLion

	ReactorTest();

protected:

	std::unique_ptr<Reactor> reactor;

	class MockEventDispatcher : public EventDispatcher
	{
	public:

		MOCK_METHOD3(addTag, void(const EventTag& tag, std::shared_ptr<EventHandler> handler, bool bubbles));
		MOCK_METHOD2(dispatchTag, void(const EventTag& tag, EventData& data));

		EventDispatcher_STL realDispatcher;

	} dispatcher;

	class MockEventHandler : public EventHandler
	{
	public:

		MOCK_METHOD2(handle, void(EventData& data, const EventTag& tag));
	};

};

ReactorTest::ReactorTest()
{
	EXPECT_CALL(dispatcher, addTag(_, _, _))
			.Times(AtLeast(0))
	        .WillRepeatedly(Invoke(&dispatcher.realDispatcher, &EventDispatcher_STL::addTag));
	EXPECT_CALL(dispatcher, dispatchTag(_, _))
			.Times(AtLeast(0))
	        .WillRepeatedly(Invoke(&dispatcher.realDispatcher, &EventDispatcher_STL::dispatchTag));
	reactor = std::unique_ptr<Reactor>(new Reactor(dispatcher));
}



TEST_F(ReactorTest, ProxiesDispatcher)
{
	EventTag Tag = EventTag::createTag("Tag");
	auto handler = std::make_shared<EventHandler>();
	EXPECT_CALL(this->dispatcher, addTag(Tag, handler, true)).Times(1);
	this->reactor->registerListener(Tag, handler, true);
}

TEST_F(ReactorTest, DefaultDispatchArgument)
{
	EventTag Tag = EventTag::createTag("Tag");
	EXPECT_CALL(this->dispatcher, dispatchTag(Tag, An<EventData&>())).Times(1);
	this->reactor->sendMessage(Tag, 0);
	this->reactor->sendMessage(Reactor::getTags().Stop, std::shared_ptr<EventData>(new EventData), 255);
	this->reactor->run();
}

TEST_F(ReactorTest, Prioritizes)
{
	EventTag LowPriTag = EventTag::createTag("Low");
	EventTag MedPriTag = EventTag::createTag("Med");
	EventTag HiPriTag = EventTag::createTag("Hi");
	{
		InSequence s;
		EXPECT_CALL(this->dispatcher, dispatchTag(HiPriTag, _));
		EXPECT_CALL(this->dispatcher, dispatchTag(MedPriTag, _));
		EXPECT_CALL(this->dispatcher, dispatchTag(LowPriTag, _));
	}
	this->reactor->sendMessage(LowPriTag, std::shared_ptr<EventData>(new EventData), 127);
	this->reactor->sendMessage(HiPriTag,  std::shared_ptr<EventData>(new EventData), 0);
	this->reactor->sendMessage(MedPriTag, std::shared_ptr<EventData>(new EventData), 63);
	this->reactor->sendMessage(Reactor::getTags().Stop, std::shared_ptr<EventData>(new EventData), 255);
	this->reactor->run();
}

TEST_F(ReactorTest, StopsOnIdle)
{
	EventTag Tag = EventTag::createTag("Tag");
	{
		EXPECT_CALL(this->dispatcher, dispatchTag(Tag, _)).Times(1);
	}
	this->reactor->sendMessage(Reactor::getTags().Stop, std::shared_ptr<EventData>(), 0);
	this->reactor->sendMessage(Tag, std::shared_ptr<EventData>(), 1);
	this->reactor->run();
}

TEST_F(ReactorTest, StopsImmediately)
{
	EventTag Tag = EventTag::createTag("Tag");
	{
		EXPECT_CALL(this->dispatcher, dispatchTag(Tag, _)).Times(0);
	}
	this->reactor->sendMessage(Reactor::getTags().StopNow, std::shared_ptr<EventData>(), 0);
	this->reactor->sendMessage(Tag, std::shared_ptr<EventData>(), 1);
	this->reactor->run();
}

class ReactorTest_IsRunning_TagHandler : public EventHandler
{
private:

	Reactor& reactor;

public:

	ReactorTest_IsRunning_TagHandler(Reactor& reactor) : reactor(reactor) {}

	void handle(EventData& data, const EventTag& tag) override
	{
		EXPECT_TRUE(reactor.isRunning()) << "Not running whilst running";
	}
};

TEST_F(ReactorTest, IsRunning)
{
	EventTag Tag = EventTag::createTag("Tag");
	EXPECT_FALSE(this->reactor->isRunning()) << "Not stopped after init";
	this->reactor->registerListener(Tag, std::make_shared<ReactorTest_IsRunning_TagHandler>(*(this->reactor)));
	EXPECT_CALL(this->dispatcher, dispatchTag(Tag, _))
			.WillOnce(Invoke(&dispatcher.realDispatcher, &EventDispatcher_STL::dispatchTag));
	this->reactor->sendMessage(Tag, 0);
	this->reactor->sendMessage(Reactor::getTags().Stop, 255);
	this->reactor->run();
	EXPECT_FALSE(this->reactor->isRunning()) << "Not stopped after exit";
}

TEST_F(ReactorTest, StartRunningEvent)
{
	EXPECT_CALL(this->dispatcher, dispatchTag(Reactor::getTags().Started, _)).Times(1);
	this->reactor->sendMessage(Reactor::getTags().Stop, 255);
	this->reactor->run();
}

TEST_F(ReactorTest, SendMessageInHandler)
{
	std::shared_ptr<MockEventHandler> h1(new MockEventHandler),
	                                  h2(new MockEventHandler);
	EventTag t1 = EventTag::createTag("1");
	EventTag t2 = EventTag::createTag("2");
	std::shared_ptr<EventData> d1(new EventData);
	std::shared_ptr<EventData> d2(new EventData);
	{
		InSequence s;
		EXPECT_CALL(*h1, handle(_, Property(&EventTag::getString, t1.getString())))
				.WillOnce(InvokeWithoutArgs([&](){
					this->reactor->sendMessage(t2, move(d2), 1);
				}));
		EXPECT_CALL(*h2, handle(_, Property(&EventTag::getString, t2.getString()))).Times(1);
	}
	this->reactor->registerListener(t1, h1);
	this->reactor->registerListener(t2, h2);
	this->reactor->sendMessage(t1, move(d1), 1);
	this->reactor->sendMessage(Reactor::getTags().Stop, std::shared_ptr<EventData>(), 255);
	this->reactor->run();
}
