#include "mevent/EventTag.h"

#include <gtest/gtest.h>

#include <string>

using namespace MEvent;
using namespace std;

TEST(EventTagTest, SubstrInheritance)
{
	EventTag Parent = EventTag::createTag("Parent");
	EventTag Child = EventTag::createTag("Child", Parent);
	ASSERT_GT(Parent.getString().size(), 0U); // Assert that both tags have something
	ASSERT_GT(Child.getString().size(), 0U);
	ASSERT_TRUE(Child.getString().find(Parent.getString()) != string::npos); // Parent tag is part of child tag
}

TEST(EventTagTest, ReflexiveEquality)
{
	EventTag Tag = EventTag::createTag("Tag");
	ASSERT_EQ(Tag, Tag);
	EventTag CTag = EventTag::createTag("C", Tag);
	ASSERT_EQ(CTag, CTag);
}

TEST(EventTagTest, ValueEquality)
{
	EventTag Tag1 = EventTag::createTag("Tag");
	EventTag Tag2 = EventTag::createTag("Tag");
	ASSERT_EQ(Tag1, Tag2);
	EventTag CTag1 = EventTag::createTag("C", Tag1);
	EventTag CTag2 = EventTag::createTag("C", Tag2);
	ASSERT_EQ(CTag1, CTag2);
}


