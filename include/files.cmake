include_directories(${CMAKE_CURRENT_LIST_DIR})

macro(add_header file)
    set(HEADER_FILES ${HEADER_FILES} ${CMAKE_CURRENT_LIST_DIR}/${file})
endmacro()

add_header(mevent.h)
add_header(mevent/EventTag.h)
add_header(mevent/EventDispatcher.h)
add_header(mevent/EventHandler.h)
add_header(mevent/EventDispatcher_STL.h)
add_header(mevent/Reactor.h)
