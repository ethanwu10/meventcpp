#pragma once

#include "mevent/EventDispatcher.h"
#include "EventTag.h"

#include <map>
#include <vector>
#include <memory>

namespace MEvent
{

class EventDispatcher_STL : public EventDispatcher
{
public:

	EventDispatcher_STL();

	void addTag(const EventTag& tag, std::shared_ptr<EventHandler> handler, bool bubbles) override;

	void dispatchTag(const EventTag& tag, EventData& data) override;

private:

	struct LUTNode
	{
		std::vector<std::shared_ptr<EventHandler>> explicitHandlers;
		std::vector<std::shared_ptr<EventHandler>> bubblingHandlers;
		std::map<std::string, LUTNode> children;
	};

	LUTNode LUTRoot;

};

}
