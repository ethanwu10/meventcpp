#pragma once

#include "mevent/EventTag.h"
#include "mevent/EventHandler.h"
#include "mevent/EventDispatcher.h"

#include <queue>
#include <vector>
#include <memory>

namespace MEvent
{

class Reactor
{
public:

	Reactor(EventDispatcher& dispatcher);

	typedef uint8_t Priority_t;

	/**
	 * Queue message into reactor
	 *
	 * @param tag Tag of message
	 * @param data Data for message
	 * @param priority Message priority - 0 is highest
	 */
	virtual void sendMessage(const EventTag& tag, std::shared_ptr<EventData> data, Priority_t priority);

	/**
	 * Queue message into reactor with an empty data object
	 *
	 * @param tag Tag of message
	 * @param priority Message priority - 0 is highest
	 */
	virtual void sendMessage(const EventTag& tag, Priority_t priority);

	/**
	 * Register a message listener
	 *
	 * @param tag Tag to listen for
	 * @param handler Handler to attach
	 * @param bubbles Whether or not this handler accepts bubbling tags
	 */
	virtual void registerListener(const EventTag& tag, const std::shared_ptr<EventHandler>& handler, bool bubbles = true);

	virtual void run();

	virtual bool isRunning();

	struct Tags
	{
		EventTag Root;

		EventTag Stop;
		EventTag StopNow;

		EventTag Started;

	private:

		friend Reactor;
		Tags();
	};
	static const Tags& getTags();

private:

	class QueuedEvent
	{
	public:

		QueuedEvent(const EventTag& tag, std::shared_ptr<EventData>&& data, Priority_t priority)
				: tag(tag), data(std::move(data)), priority(priority) {}

		QueuedEvent(QueuedEvent&& other);
		QueuedEvent& operator=(QueuedEvent&& other);

		EventTag tag;
		std::shared_ptr<EventData> data;
		Priority_t priority;

	};
	struct QueuedEventComparator : std::binary_function<QueuedEvent, QueuedEvent, bool>
	{
		bool operator()(const QueuedEvent& e1, const QueuedEvent& e2) { return e1.priority > e2.priority; }
	};

	std::priority_queue<QueuedEvent, std::vector<QueuedEvent>, QueuedEventComparator> queue;

	EventDispatcher& dispatcher;


	bool shouldStop = false, shouldStopNow = false;
	bool running = false;


	class StopHandler : public EventHandler
	{
	public:

		StopHandler(Reactor& parent) : parent(parent) {}

		void handle(EventData& data, const EventTag& tag) override;

	protected:

		Reactor& parent;
	};

};

}

