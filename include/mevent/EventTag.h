#pragma once

#include <string>

namespace MEvent
{

class EventTag
{
public:

	EventTag() = delete;

	inline std::string getString() const { return tag; }

	inline bool operator==(const EventTag& that) const { return this->getString() == that.getString(); }
	inline bool operator!=(const EventTag& that) const { return !(*this == that); }

	static EventTag createTag(std::string name, EventTag base);
	static EventTag createTag(std::string name);

	static const EventTag& getRoot();
	static const std::string& getSeparator();

private:

	std::string tag;

	explicit inline EventTag(std::string tag) : tag(tag) {}

};

}
