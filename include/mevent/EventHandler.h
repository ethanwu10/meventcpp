#pragma once

#include "mevent/EventTag.h"

namespace MEvent
{

class EventData
{
public:

	virtual ~EventData() {};
};

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

class EventHandler
{
public:

	virtual ~EventHandler() {};

	virtual void handle(EventData& data, const EventTag& tag) {};

};

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif

}
