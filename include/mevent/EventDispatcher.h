#pragma once

#include "mevent/EventTag.h"
#include "mevent/EventHandler.h"

#include <memory>

namespace MEvent
{

class EventDispatcher
{
public:

	virtual ~EventDispatcher() {};

	/**
	 * Add handler to tag
	 *
	 * @param tag Tag to register to
	 * @param handler Handler to register
	 * @param bubbles Whether or not this handler accepts bubbling tags
	 */
	virtual void addTag(const EventTag& tag, std::shared_ptr<EventHandler> handler, bool bubbles = true) = 0;

	/**
	 * Dispatches a tag
	 *
	 * @param tag Tag to dispatch
	 * @param data Data to pass
	 */
	virtual void dispatchTag(const EventTag& tag, EventData& data) = 0;

};

}
