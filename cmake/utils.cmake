# Adapted from internal_utils.cmake of Google Tests

macro(config_flags)

    set(cxx_base_flags "-std=c++11 -Wall")
    set(cxx_strict_flags "-Wextra")

    set(cxx_default "${CMAKE_CXX_FLAGS} ${cxx_base_flags}")
    set(cxx_strict "${cxx_default} ${cxx_strict_flags}")
endmacro()

function(c_cxx_library name cxx_flags)
    add_library(${name} ${ARGN})
    if (cxx_flags)
        set_target_properties(${name}
                PROPERTIES
                COMPILE_FLAGS "${cxx_flags}")
    endif()
endfunction()

function(c_cxx_executable name cxx_flags libs)
    add_executable(${name} ${ARGN})
    if (cxx_flags)
        set_target_properties(${name}
                PROPERTIES
                COMPILE_FLAGS "${cxx_flags}")
    endif()
    foreach (lib ${libs})
        target_link_libraries(${name} ${lib})
    endforeach()
endfunction()
